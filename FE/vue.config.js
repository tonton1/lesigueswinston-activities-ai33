const path = require('path');  
 module.exports = {  
 configureWebpack: {  
     resolve: {  
       alias: {  
          //Navigations
         '@Navs': path.resolve(__dirname, 'src/components/navigations'),
         //Pages
         '@Pages': path.resolve(__dirname, 'src/components/views'),
         //ChartData
         '@Chartdata': path.resolve(__dirname, 'src/components/charts'),
         //Images
         '@Images': path.resolve(__dirname, 'src/assets/images'),
         
       },  

     },  
     
 },
 
}
