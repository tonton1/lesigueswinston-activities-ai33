import axios from 'axios'

const books = {
    namespaced:true,
    state: () => ({

        booksData:[],

    }),
    mutations:{

        SET_BOOKS_DATA(state, data)
        {
            state.booksData =  data
        },

    },
    actions:{

        getBooksData({commit})
        {
            axios.get('http://127.0.0.1:8000/api/books')
            .then(res => {
                commit('SET_BOOKS_DATA' , res.data)
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        addBooksData({dispatch},books)
        {
            axios.post('http://127.0.0.1:8000/api/books', books)
            .then(res => {
                dispatch('getBooksData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        updateBooksData({dispatch},books)
        {
            axios.put(`http://127.0.0.1:8000/api/dashboard/${books.id}`, books)
            .then(res => {
                dispatch('getBooksData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        deleteBooksData({dispatch},books)
        {
            axios.delete(`http://127.0.0.1:8000/api/books/${books.id}`)
            .then( (res) => {
                dispatch('getBooksData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        }

    },
    getters:{
        
        data(state)
        {   
            return state.booksData.reverse()
        }

    }

}

export default books