import Vue from 'vue'
import router from './router'
import App from './App.vue'
import toastr from '@/assets/js/index.js'

Vue.config.productionTip = false

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import "toastr/build/toastr.css";
import store from './store'

Vue.use(toastr);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
