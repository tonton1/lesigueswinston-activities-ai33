<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(BooksTableSeeder::class);
         $this->call(BorrowedBooksTableSeeder::class);
         $this->call(CategoriesTableSeeder::class);
         $this->call(PatronsTableSeeder::class);
         $this->call(ReturnedBooksTableSeeder::class);
    }
}
