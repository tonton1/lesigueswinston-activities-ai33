<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:: table('pages')->insert([
            'id' => '1801962',
            'category' => 'Sci-fi','Romance','action',
            'book_id' => '2',
            'patron_id' => '3',
        ]);
    }
}
