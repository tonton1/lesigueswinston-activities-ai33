<?php

namespace App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class returned_book extends Model
{
    use HasFactory;
    protected $fillable =['name','author','copies','category_id'];

    public function category()
    {
        return $this->hasMany(category::class,'category_id','id');
    }
    public function borrowed_book()
    {
        return $this->hasMany(borrowed_book::class,'book_id', 'id');
    }
    public function returned_book()
    {
        return $this->hasMany(returned_book::class,'book_id', 'id');
    }
    public function patron()
    {
        return $this->hasMany(patron::class,'book_id', 'id');
    }
    public function book()
    {
        return $this->hasMany(book::class,'book_id', 'id');
    }
}