<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class borrowed_book extends Model
{
    

    public function category()
    {
        return $this->belongsTo('App\category');
    }
    public function borrowed_book()
    {
        return $this->belongsTo('App\borrowed_book');
    }
    public function returned_book()
    {
        return $this->belongsTo('App\returned_book');
    }
    public function patron()
    {
        return $this->belongsTo('App\patron');
    }
    public function book()
    {
        return $this->belongsTo('App\book');
    }
}
