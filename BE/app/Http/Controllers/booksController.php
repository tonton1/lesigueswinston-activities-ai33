<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class booksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Books = Book::all();
        return response()->json([$Books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $BOOKS  = new book();

        $BOOKS->name = $request->name;
        $BOOKS->author = $request->author;
        $BOOKS->copies = $request->copies;
        $BOOKS->category_id = $request->category_id;

        $BOOKS->save();
        return response()->json($BOOKS);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $BOOKS  = book::find($id);

        $BOOKS->name = $request->name;
        $BOOKS->author = $request->author;
        $BOOKS->copies = $request->copies;
        $BOOKS->category_id = $request->category_id;

        $BOOKS->update();
        return response()->json($BOOKS);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $BOOKS = book::find($id);
        $BOOKS->delete();
        return response()->json($BOOKS);
    }
}
