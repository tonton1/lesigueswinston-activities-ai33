<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class returned_booksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Returned_books = returned_books::all();
        return response()->json([$Returned_books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Returned_books  = new Returned_books();

        $Returned_books->id = $request->id;
        $Returned_books->name = $request->name;
        $Returned_books->patron_id = $request->author;
        $Returned_books->copies = $request->copies;
        $Returned_books->books_id = $request->category_id;

        $Returned_books->save();
        return response()->json($Returned_books);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Returned_books  = Returned_books::find($id);

        $Returned_books->id = $request->id;
        $Returned_books->name = $request->name;
        $Returned_books->patron_id = $request->author;
        $Returned_books->copies = $request->copies;
        $Returned_books->books_id = $request->category_id;

        $Returned_books->update();
        return response()->json($Returned_books);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Returned_books = book::find($id);
        $Returned_books->delete();
        return response()->json($Returned_books);
    }
}
