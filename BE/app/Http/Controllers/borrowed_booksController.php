<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class borrowed_booksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Borrowed_books = Borrowed_books::all();
        return response()->json([$Borrowed_books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Borrowed_books  = new borrowed_books();

        $Borrowed_books->id = $request->id;
        $Borrowed_books->name = $request->name;
        $Borrowed_books->patron_id = $request->author;
        $Borrowed_books->copies = $request->copies;
        $Borrowed_books->books_id = $request->category_id;

        $Borrowed_books->save();
        return response()->json($Borrowed_books);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Borrowed_books  = borrowed_books::find($id);

        $Borrowed_books->id = $request->id;
        $Borrowed_books->name = $request->name;
        $Borrowed_books->patron_id = $request->author;
        $Borrowed_books->copies = $request->copies;
        $Borrowed_books->books_id = $request->category_id;

        $Borrowed_books->update();
        return response()->json($Borrowed_books);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Borrowed_books = book::find($id);
        $Borrowed_books->delete();
        return response()->json($Borrowed_books);
    }
}
