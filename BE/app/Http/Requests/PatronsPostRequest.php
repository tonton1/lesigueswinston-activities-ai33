<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatronsPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::user()->can('manage-users')){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|unique:posts|max:100',
            'name' => 'required',
            'patron_id' => 'required',
            'copies' => 'integer|size:10',
            'books_id' => 'required',
        ];
    }
}
