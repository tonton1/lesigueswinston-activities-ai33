<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BooksPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::user()->can('manage-users')){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:posts|max:100',
            'author' => 'required',
            'copies' => 'integer|size:10',
            'category_id' => 'required',
        ];
    }
}


