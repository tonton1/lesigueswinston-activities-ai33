<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resources([
    'books' => booksController::class,
    'returned_books' => returned_booksController::class,
    'categories' => categoriesController::class,
    'borrowed_books' => borrowed_booksController::class,
    'patrons' => patronController::class,
]);
    